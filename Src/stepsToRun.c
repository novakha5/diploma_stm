/**
  ******************************************************************************
  * @file           : stepsToRun.c
  * @brief          : File for initialize, store, change and read the steps
  * 				  which needs to be run.
  ******************************************************************************
  * Created on: 23. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/


#include "stepsToRun.h"
#include "parser.h"

volatile Steps_t StepsToRun;
volatile Steps_remain_t RemainingSteps;
/**
 * @brief Set steps and remaining steps value to 0
 * */
void InitSteps()
{
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		StepsToRun.steps_x = 0;
		StepsToRun.steps_y = 0;
		StepsToRun.steps_z = 0;
		StepsToRun.steps_fi = 0;
		StepsToRun.enable_x = 0;
		StepsToRun.enable_y = 0;
		StepsToRun.enable_z = 0;
		StepsToRun.enable_fi = 0;
		osMutexRelease(stepsMutex);
	}
	if(osMutexWait(remainingMutex, osWaitForever) == osOK)
	{
		RemainingSteps.steps_x = 0;
		RemainingSteps.steps_y = 0;
		RemainingSteps.steps_z = 0;
		RemainingSteps.steps_fi = 0;
		osMutexRelease(remainingMutex);
	}
}

/**
 * @brief Tells if there are remaining steps
 * @retval 1 if there are any steps remaining, otherwise 0
 * */
int AnyStepsSet()
{
	int ret;
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		ret = (StepsToRun.steps_x != 0 ||  StepsToRun.steps_y != 0 || StepsToRun.steps_z != 0 || StepsToRun.steps_fi != 0);
		osMutexRelease(stepsMutex);
	}
	return ret;
}

/**
 * @brief Give structure with steps remaining
 * @retval Structure with not finished steps
 * */
Steps_t GetWaitingSteps()
{
	Steps_t ret;
	ret.steps_x = 0;
	ret.enable_x = 0;
	ret.steps_y = 0;
	ret.enable_y = 0;
	ret.steps_z = 0;
	ret.enable_z = 0;
	ret.steps_fi = 0;
	ret.enable_fi = 0;
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		ret.steps_x = StepsToRun.steps_x;
		ret.enable_x = StepsToRun.enable_x;
		ret.steps_y = StepsToRun.steps_y;
		ret.enable_y = StepsToRun.enable_y;
		ret.steps_z = StepsToRun.steps_z;
		ret.enable_z = StepsToRun.enable_z;
		ret.steps_fi = StepsToRun.steps_fi;
		ret.enable_fi = StepsToRun.enable_fi;
		osMutexRelease(stepsMutex);
	}
	return ret;
}

/**
 * @brief Sets steps to run for all axes if there are no running steps
 * @param x: Steps for axis x
 * @param y: Steps for axis y
 * @param z: Steps for axis z
 * @param fi: Steps for axis fi
 * @retval stepsOK, if the steps has been set,
 * stepsStillRunning if there are any remaining and the new one are not all zero,
 * stepsOutOfBounderies if the steps will lead to place out of the CNC
 * */
stepsSucces_t SetAllSteps(int32_t x, int32_t y, int32_t z, int32_t fi)
{
	if(AnyStepsSet())
	{
		/*Check if it is not for stop the machine*/
		if(x != 0 || y != 0 || z != 0 || fi != 0)
		{
			return stepsStillRunning;
		}
	}

	Position pos = GetGlobalPostition();
	if( (pos.x+x) > MACHINE_RANGE_X || (pos.x+x) < 0 || (pos.y+y) > MACHINE_RANGE_Y || (pos.y+y) < 0 ||
	    (pos.z+z) > MACHINE_RANGE_Z || (pos.z+z) < 0 || (pos.fi+fi) > MACHINE_RANGE_FI || (pos.fi+fi) < 0 )
	{
		return stepsOutOfBoundaries;
	}

	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		StepsToRun.steps_x = x;
		StepsToRun.steps_y = y;
		StepsToRun.steps_z = z;
		StepsToRun.steps_fi = fi;
		osMutexRelease(stepsMutex);
	}

	if(!AnyStepsSet())
	{
		sendPosition();
	}
	return stepsOK;
}

/**
 * @brief Set all values of StepsToRun to 0
 * */
void ClearAllSteps()
{
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		StepsToRun.steps_x = 0;
		StepsToRun.steps_y = 0;
		StepsToRun.steps_z = 0;
		StepsToRun.steps_fi = 0;
		StepsToRun.enable_x = 0;
		StepsToRun.enable_y = 0;
		StepsToRun.enable_z = 0;
		StepsToRun.enable_fi = 0;
		osMutexRelease(stepsMutex);
	}
}

/**
 * @brief Set enable value in StepsToRun to state for given axis
 * @param axis: For which axis should be steps enabled/disabled
 * @param state: 1 for enable, 0 for disable
 * @return stepsOK, if enabled, stepsError if the axis or state is invalid
 * */
stepsSucces_t SetEnableDisableAxis(axes_t axis, uint8_t state)
{
	stepsSucces_t ret = stepsOK;
	if(state < 0 || state > 1)
	{
		ret = stepsError;
	}
	else if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		switch(axis)
		{
		case axis_x:
			StepsToRun.enable_x = state;
			break;
		case axis_y:
			StepsToRun.enable_y = state;
			break;
		case axis_z:
			StepsToRun.enable_z = state;
			break;
		case axis_fi:
			StepsToRun.enable_fi = state;
			break;
		default:
			ret = stepsError;
		}
		osMutexRelease(stepsMutex);
	}
	return ret;
}

/**
 * @brief Set enable value in StepsToRun to 0 for given axis
 * @param axis: For which axis should be steps enabled
 * @return stepsOK, if disabled, stepsError if the axis is invalid
 * */
stepsSucces_t SetDisableAxis(axes_t axis)
{
	stepsSucces_t ret = stepsOK;
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		switch(axis)
		{
		case axis_x:
			StepsToRun.enable_x = 0;
			break;
		case axis_y:
			StepsToRun.enable_y = 0;
			break;
		case axis_z:
			StepsToRun.enable_z = 0;
			break;
		case axis_fi:
			StepsToRun.enable_fi = 0;
			break;
		default:
			ret = stepsError;
		}
		osMutexRelease(stepsMutex);
	}
	return ret;
}


/**
 * @brief Sets steps for one axis only
 * @param steps: number of steps to set
 * @param axis: for which axis the steps should be set
 * @retval stepsOK if the steps has been set,
 * stepsOutOfBounderies if the steps will lead to place out of the CNC
 * */
stepsSucces_t SetSingleSteps(int32_t steps, axes_t axis)
{
	int ret = 0;
	Position pos = GetGlobalPostition();
	/*Check if the number of steps will lead out of range*/
	if(pos.homed)
	{
		switch(axis)
		{
		case axis_x:
			if( (pos.x+steps) > MACHINE_RANGE_X || (pos.x+steps) < 0)
			{
				ret = 1;
			}
			break;
		case axis_y:
			if( (pos.y+steps) > MACHINE_RANGE_Y || (pos.y+steps) < 0)
			{
				ret = 1;
			}
			break;
		case axis_z:
			if( (pos.z+steps) > MACHINE_RANGE_Z || (pos.z+steps) < 0)
			{
				ret = 1;
			}
			break;
		case axis_fi:
			if( (pos.fi+steps) > MACHINE_RANGE_FI || (pos.fi+steps) < 0)
			{
				ret = 1;
			}
			break;
		default:
			return stepsError;
			break;
		}
	}
	if(ret) return stepsOutOfBoundaries;

	/*Set the steps for given axis*/
	if(osMutexWait(stepsMutex, osWaitForever) == osOK)
	{
		switch(axis)
		{
		case axis_x:
			StepsToRun.steps_x = steps;
			break;
		case axis_y:
			StepsToRun.steps_y = steps;
			break;
		case axis_z:
			StepsToRun.steps_z = steps;
			break;
		case axis_fi:
			StepsToRun.steps_fi = steps;
			break;
		default:
			return stepsError;
			break;
		}
		osMutexRelease(stepsMutex);
	}

	if(!AnyStepsSet())
	{
		sendPosition();
	}
	return stepsOK;
}

/**
 * @brief Set remaining steps for one axis
 * @param remaining: number of remaining steps to set
 * @param axis:  for which axis the remaining steps should be set
 * @retval stepsOK, if the steps has been set, otherwise stepsError
 * */
stepsSucces_t SetRemaining(int32_t remaining, axes_t axis)
{
	if(remaining < 0) return stepsError;
	stepsSucces_t ret = stepsOK;
	if(osMutexWait(remainingMutex, osWaitForever) == osOK)
	{
		switch(axis)
		{
		case axis_x:
			RemainingSteps.steps_x = remaining;
			break;
		case axis_y:
			RemainingSteps.steps_y = remaining;
			break;
		case axis_z:
			RemainingSteps.steps_z = remaining;
			break;
		case axis_fi:
			RemainingSteps.steps_fi = remaining;
			break;
		default:
			ret = stepsError;
			break;
		}
		osMutexRelease(remainingMutex);
	}
	return ret;
}

/**
 * @brief Get structure with steps remaining to place to the counter
 * @retval Structure with remaining steps
 */
Steps_remain_t GetRemainingSteps()
{
	Steps_remain_t ret;
	ret.steps_x = 0;
	ret.steps_y = 0;
	ret.steps_z = 0;
	ret.steps_fi = 0;
	if(osMutexWait(remainingMutex, osWaitForever) == osOK)
	{
		ret.steps_x = RemainingSteps.steps_x;
		ret.steps_y = RemainingSteps.steps_y;
		ret.steps_z = RemainingSteps.steps_z;
		ret.steps_fi = RemainingSteps.steps_fi;
		osMutexRelease(remainingMutex);
	}
	return ret;
}
