/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <stdio.h>
#include <stdint.h>

#include "main.h"
#include "cmsis_os.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "init.h"
#include "queue.h"
#include "position.h"

#include "motors.h"
#include "parser.h"

#include <string.h>

#define MAX_LINE_LEN 32 /*The maximum length of message from PC should be 24 characters*/

/* Private variables ---------------------------------------------------------*/
IWDG_HandleTypeDef hiwdg;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim9;
TIM_HandleTypeDef htim11;

osThreadId USBTaskHandle;
osThreadId wdgTaskHandle;
osThreadId motorTaskHandle;

QueueHandle_t USBQueue;
uint8_t ucQueueArea[USB_QUEUE_SIZE * sizeof(uint8_t)];
static StaticQueue_t xStaticQueue;

/*Store actual position of CNC*/
volatile osMutexId positionMutex;
volatile osMutexId sensorMutex;
volatile osMutexId stepsMutex;
volatile osMutexId remainingMutex;

/* Private function prototypes -----------------------------------------------*/

void StartUSBCommunication(void const * argument);
void StartMotorTimers(void const * argument);
void iwdgStart(void const * argument);

/**
 * @brief For debugging purposes only:
 * 		  This function disable counting of watchdog when the breakpoint or stepping is enabled
 * 		  It will also disable timers while the breakpoint or stepping is enabled
 * */
void dbgStop() {
	DBGMCU->APB1FZ |= (DBGMCU_APB1_FZ_DBG_TIM2_STOP
			| DBGMCU_APB1_FZ_DBG_TIM3_STOP |
			DBGMCU_APB1_FZ_DBG_TIM4_STOP);

	DBGMCU->APB2FZ |= (DBGMCU_APB2_FZ_DBG_TIM1_STOP
			| DBGMCU_APB2_FZ_DBG_TIM9_STOP |
			DBGMCU_APB2_FZ_DBG_TIM11_STOP);
}

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/*bug freeze for watchdog to not run during debugging - breakpoint and steps*/
	__HAL_DBGMCU_FREEZE_IWDG();
	dbgStop();
	/* MCU Configuration--------------------------------------------------------*/
	System_init();

	/*Mutex for global position*/
	osMutexDef(positionMutex);
	positionMutex = osMutexCreate(osMutex(positionMutex));

	/*Mutex for end sensors*/
	osMutexDef(sensorMutex);
	sensorMutex = osMutexCreate(osMutex(sensorMutex));

	/*Mutex for steps setting*/
	osMutexDef(stepsMutex);
	stepsMutex = osMutexCreate(osMutex(stepsMutex));

	/*Mutex for remaining steps*/
	osMutexDef(remainingMutex);
	remainingMutex = osMutexCreate(osMutex(remainingMutex));

	/*Initialization of the global position
	 *it is just starting point - the homing needs to be done*/
	InitGlobalPosition(0);

	/* definition and creation of USB communication */
	osThreadDef(USBCmm, StartUSBCommunication, osPriorityNormal, 0, 2048);
	USBTaskHandle = osThreadCreate(osThread(USBCmm), NULL);

	/* definition and creation of motor handling */
	osThreadDef(MotorTask, StartMotorTimers, osPriorityNormal, 0, 2048);
	motorTaskHandle = osThreadCreate(osThread(MotorTask), NULL);

	/*Start Watchdog thread*/
	osThreadDef(wdgTask, iwdgStart, osPriorityNormal, 0, 64);
	wdgTaskHandle = osThreadCreate(osThread(wdgTask), NULL);

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	/* Infinite loop */
	while (1) {
	}
}

/**
 * @brief  Function implementing the start of the USB communication.
 * @param  argument: Not used
 * */
void iwdgStart(void const * argument) {
	while (1) {
		HAL_IWDG_Refresh(&hiwdg);
		osDelay(100);
	}
}

/**
 * @brief  Function implementing the USB communication.
 * @param  argument: Not used
 */
void StartUSBCommunication(void const * argument) {
	/* init code for USB_DEVICE */
	MX_USB_DEVICE_Init();
	/*Creates queue for USB communication*/
	USBQueue = xQueueCreateStatic(USB_QUEUE_SIZE, sizeof(uint8_t), ucQueueArea,
			&xStaticQueue);
	configASSERT(USBQueue);

	uint8_t Msg[MAX_LINE_LEN + 1];
	int len;
	int endOfLine = 0;
	int lineLen = 0;
	BaseType_t status;

	/* Infinite loop */
	while (1) {
		/*Queue version of reading the USB*/
		len = uxQueueMessagesWaiting(USBQueue);
		if (len > 0) {
			/*For this example the message as maximum value of MAX_LINE_LEN*/
			if (lineLen + len > MAX_LINE_LEN) {
				len = MAX_LINE_LEN - lineLen;
			}
			/*Receive all characters - max to size of message buffer*/
			for (int msgPos = 0; msgPos < len; msgPos++) {
				status = xQueueReceive(USBQueue, &Msg[lineLen], 100);
				/*No data has been received*/
				if (status != pdTRUE) {
					break;
				}
				lineLen++;
				/*Find the end of line*/
				if (Msg[lineLen - 1] == '\r') {
					endOfLine = 1;
					break;
				}
			}
			/*Check if there is complete line to sent to parser*/
			if (endOfLine) {
				Msg[lineLen] = '\0';
				/*Parse the message*/
				parseLine(Msg);
				/*Reset variables for next lines*/
				lineLen = 0;
				endOfLine = 0;
				len = 0;
			}
			/*The message is longer without \r that it should be
			 * Discard the message since it is not useful
			 * It could cause the problem while reading of the next one*/
			else if (lineLen >= MAX_LINE_LEN) {
				for (int iterate = 0; iterate <= MAX_LINE_LEN; iterate++) {
					Msg[iterate] = '\0';
				}
				lineLen = 0;
				endOfLine = 0;
				len = 0;
			}
		}
		osDelay(1);
	}
}

/**
 * @brief  Function implementing the motorTimers thread.
 * @param  argument: Not used
 */
void StartMotorTimers(void const * argument) {
	initMotors();
	InitSteps();

	homeMotors();
	/* Infinite loop */
	for (;;) {
		if(getRequestHomePos())
		{
			homeMotors();
		}
		else
		{
			controlMotors();
		}
		osDelay(10);
	}
}

/**
 * @brief  Period elapsed callback in non blocking mode
 * @note   This function is called  when TIM10 interrupt took place, inside
 * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
 * a global variable "uwTick" used as application time base.
 * @param  htim : TIM handle
 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == TIM10) {
		HAL_IncTick();
	}
}


/**
 * @brief Callback function for EXTI inputs, it resolves the end switches rising edge
 * @param GPIO_Pin: pin value
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
	manualState_t manual = getManualState();
	if(manual.isManualState)
	{
		// Case under manual positioning by user
		switch (GPIO_Pin) {
		case GPIO_PIN_10: //LIMIT_Y
			if(manual.manualAxis == axis_y)
			{
				// End the manual state
				setManualState(0, 0, axis_unspecified);
				if(manual.manualDir < 0)
				{
					SetGlobalPostition(-1, 0, -1, -1);
					setEndOfLine(axis_y, 1);
				}
				else if(manual.manualDir > 0)
				{
					SetGlobalPostition(-1, MACHINE_RANGE_Y, -1, -1);
					setEndOfLine(axis_y, 2);
				}
				HAL_TIM_Base_Stop_IT(&htim9);
				__HAL_TIM_SET_COUNTER(&htim9, 0);
				htim2.Instance->ARR = 0;
				sendPosition();
				return;
			}
			break;
		case GPIO_PIN_11: //LIMIT_Z
			if(manual.manualAxis == axis_z)
			{
				// End the manual state
				setManualState(0, 0, axis_unspecified);
				if(manual.manualDir < 0)
				{
					SetGlobalPostition(-1, -1, 0, -1);
					setEndOfLine(axis_z, 1);
				}
				else if(manual.manualDir > 0)
				{
					SetGlobalPostition(-1, -1, MACHINE_RANGE_Z, -1);
					setEndOfLine(axis_z, 2);
				}
				HAL_TIM_Base_Stop_IT(&htim4);
				__HAL_TIM_SET_COUNTER(&htim4, 0);
				htim3.Instance->ARR = 0;
				sendPosition();
				return;
			}
			break;
		case GPIO_PIN_12: //LIMIT_FI
			if(manual.manualAxis == axis_fi)
			{
				// End the manual state
				setManualState(0, 0, axis_unspecified);
				if(manual.manualDir < 0)
				{
					SetGlobalPostition(-1, -1, -1, 0);
					setEndOfLine(axis_fi, 1);
				}
				else if(manual.manualDir > 0)
				{
					SetGlobalPostition(-1, -1, -1, MACHINE_RANGE_FI);
					setEndOfLine(axis_fi, 2);
				}
				HAL_TIM_Base_Stop_IT(&htim9);
				__HAL_TIM_SET_COUNTER(&htim9, 0);
				htim11.Instance->ARR = 0;
				sendPosition();
				return;
			}
			break;
		case GPIO_PIN_15:  //LIMIT_X
			if(manual.manualAxis == axis_x)
			{
				// End the manual state
				setManualState(0, 0, axis_unspecified);
				if(manual.manualDir < 0)
				{
					SetGlobalPostition(0, -1, -1, -1);
					setEndOfLine(axis_x, 1);
				}
				else if(manual.manualDir > 0)
				{
					SetGlobalPostition(MACHINE_RANGE_X, -1, -1, -1);
					setEndOfLine(axis_x, 2);
				}
				HAL_TIM_Base_Stop_IT(&htim4);
				__HAL_TIM_SET_COUNTER(&htim4, 0);
				htim1.Instance->ARR = 0;
				sendPosition();
				return;
			}
			break;
		default:
			break;
		}
	}
	Steps_t steps = GetWaitingSteps();
	switch (GPIO_Pin) {
	case GPIO_PIN_10: //LIMIT_Y
		if (steps.steps_y <= 0) {
			SetGlobalPostition(-1, 0, -1, -1);
			setEndOfLine(axis_y, 1);
		} else {
			SetGlobalPostition(-1, MACHINE_RANGE_Y, -1, -1);
			setEndOfLine(axis_y, 2);
		}
		HAL_TIM_Base_Stop_IT(&htim9);
		__HAL_TIM_SET_COUNTER(&htim9, 0);
		htim2.Instance->ARR = 0;
		SetSingleSteps(0, axis_y);
		SetRemaining(0, axis_y);
		break;
	case GPIO_PIN_11: //LIMIT_Z
		if (steps.steps_z <= 0) {
			SetGlobalPostition(-1, -1, 0, -1);
			setEndOfLine(axis_z, 1);
		} else {
			SetGlobalPostition(-1, -1, MACHINE_RANGE_Z, -1);
			setEndOfLine(axis_z, 2);
		}
		HAL_TIM_Base_Stop_IT(&htim4);
		__HAL_TIM_SET_COUNTER(&htim4, 0);
		htim3.Instance->ARR = 0;
		SetSingleSteps(0, axis_z);
		SetRemaining(0, axis_z);
		break;
	case GPIO_PIN_12: //LIMIT_FI
		if (steps.steps_fi <= 0) {
			SetGlobalPostition(-1, -1, -1, 0);
			setEndOfLine(axis_fi, 1);
		} else {
			SetGlobalPostition(-1, -1, -1, MACHINE_RANGE_FI);
			setEndOfLine(axis_fi, 2);
		}
		HAL_TIM_Base_Stop_IT(&htim9);
		__HAL_TIM_SET_COUNTER(&htim9, 0);
		htim11.Instance->ARR = 0;
		SetSingleSteps(0, axis_fi);
		SetRemaining(0, axis_fi);
		break;
	case GPIO_PIN_15:  //LIMIT_X
		if (steps.steps_x <= 0) {
			SetGlobalPostition(0, -1, -1, -1);
			setEndOfLine(axis_x, 1);
		} else {
			SetGlobalPostition(MACHINE_RANGE_X, -1, -1, -1);
			setEndOfLine(axis_x, 2);
		}
		HAL_TIM_Base_Stop_IT(&htim4);
		__HAL_TIM_SET_COUNTER(&htim4, 0);
		htim1.Instance->ARR = 0;
		SetSingleSteps(0, axis_x);
		SetRemaining(0, axis_x);
		break;
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 */
void Error_Handler(void) {
	osDelay(1);
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
