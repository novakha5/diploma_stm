/**
  ******************************************************************************
  * @file           : parser.c
  * @brief          : File for parsing the incoming string from USB.
  * 				  Also contains functions for sending error message or
  * 				  current global position.
  ******************************************************************************
  * Created on: 12. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/


#include "parser.h"
#include "usbd_cdc_if.h"
#include "motors.h"

 /**
  * @brief Takes incoming line, parse it and sets the position
  * @param line: Line to parse
  */
void parseLine(const uint8_t* line)
{
	if(AnyStepsSet())
	{
		uint8_t Msg[] = "Motors are busy\r";
		CDC_Transmit_FS(Msg, myStrlen(Msg));
		return;
	}

	char delim = ',';
	char splited[NUMBER_OF_AXES][MAX_NUM_LEN];
	int numberOfParsed;

	/*Split the line by ','*/
	myStrtok(splited, line, delim, &numberOfParsed);

	if(numberOfParsed == 1)
	{
		parseManualMessage(splited[0]);
		return;
	}
	/*Control, that there is right number of split parts*/
	if(numberOfParsed != NUMBER_OF_AXES)
	{
		errorMessage();
		return;
	}

	char *end;
	long int x, y, z, fi;
	/*Interpret the strings if there are numbers, check their format*/
	x = myDecadicStrtol(splited[0], &end);
	if(end == splited[0] || (*end != '\0' && *end != '\r')){
		errorMessage();
		return;
	}
	y = myDecadicStrtol(splited[1], &end);
	if(end == splited[1] || (*end != '\0' && *end != '\r')){
		errorMessage();
		return;
	}
	z = myDecadicStrtol(splited[2], &end);
	if(end == splited[2] || (*end != '\0' && *end != '\r')){
		errorMessage();
		return;
	}
	fi = myDecadicStrtol(splited[3], &end);
	if(end == splited[3] || (*end != '\0' && *end != '\r')){
		errorMessage();
		return;
	}

	/*Check the boundaries*/
	if(x < 0 || x > MACHINE_RANGE_X || y < 0 || y > MACHINE_RANGE_Y || z < 0 || z > MACHINE_RANGE_Z || fi < 0 || fi > MACHINE_RANGE_FI)
	{
		uint8_t Msg[] = "Position is out of boundaries\r";
		CDC_Transmit_FS(Msg, myStrlen(Msg));
		return;
	}

	/*Calculate steps*/
	Position pos = GetGlobalPostition();
	int32_t stepsX, stepsY, stepsZ, stepsFi;
	stepsX = x - pos.x;
	stepsY = y - pos.y;
	stepsZ = z - pos.z;
	stepsFi = fi - pos.fi;

	stepsSucces_t ret = SetAllSteps(stepsX, stepsY, stepsZ, stepsFi);
	if (ret != stepsOK)
	{

		if(ret == stepsStillRunning)
		{
			uint8_t Msg[] = "Motors are busy\r";
			CDC_Transmit_FS(Msg, myStrlen(Msg));
		}
		else
		{
			uint8_t Msg[] = "Position is out of boundaries\r";
			CDC_Transmit_FS(Msg, myStrlen(Msg));
		}
		return;
	}
	if(stepsX != 0) SetEnableDisableAxis(axis_x, 1);
	if(stepsY != 0) SetEnableDisableAxis(axis_y, 1);
	if(stepsZ != 0) SetEnableDisableAxis(axis_z, 1);
	if(stepsFi != 0) SetEnableDisableAxis(axis_fi, 1);
}

/**
 * @param dest: Array where to put split parts
 * @param strToParse: String to parse
 * @param delim: Delimit by which string should be split
 * @param num: Place where to store information about number of split parts
 */
void myStrtok(char dest[NUMBER_OF_AXES][MAX_NUM_LEN], const uint8_t* strToParse, char delim, int* num)
{
	int len = myStrlen(strToParse);
	int strPos = 0;
	int wordNum = 0; //Which split part is being written
	int charNum = 0; //Which character of the split part is being written
	while(strToParse[strPos] != '\r' && strPos < len)
	{
		if(strToParse[strPos] != delim)
		{
			if(charNum >= MAX_NUM_LEN-1) //Number is longer then it should be (wrong format)
			{
				*num = -1;
				return;
			}
			dest[wordNum][charNum] = strToParse[strPos];
			charNum++;
		}
		else //Find the delimiter - close the string, start new split part
		{
			dest[wordNum][charNum] = '\0';
			wordNum++;
			charNum = 0;
		}
		strPos++;
	}
	dest[wordNum][charNum] = '\0';
	*num = wordNum+1; //How many split parts was found (count from 0 so +1 is needed);
}

/**
 * @brief Convert string value to long integer if there are numbers from beginning
 * @param str: String to convert
 * @param end: Pointer to store next position after conversion
 * @retval Converted number if found any, otherwise 0
 */
long myDecadicStrtol(const char *str, char **end)
{
	long result = 0;
	int len = myStrlen((uint8_t *) str);
	int sign = 1;
	*end = NULL;
	for(int strPos = 0; strPos < len; strPos++)
	{
		if(str[strPos]>='0' && str[strPos]<='9') /*Number characters*/
		{
			result = 10*result + str[strPos]-'0';
		}
		else if(strPos == 0 && str[strPos]=='-') /*Sign character found*/
		{
			sign = -1;
		}
		 /*Other then number or sign character was found - end the conversion*/
		else
		{
			*end = (char *) &str[strPos];
			break;
		}
	}
	/*If whole string was number then set the end position to next*/
	if(*end == NULL )
	{
		*end = (char *) &str[len];
	}

	return result*sign; /*Apply sign if found*/
}

 /**
  * @brief Counts number of characters till '\0'
  * @param str: Character array to count
  * @retval Length of string
  */
int myStrlen(const uint8_t* str)
{
	int len = 0;
	while(str[len] != '\0')
	{
		len++;
	}
	return len;
}

/**
 * @brief Write the error to USB
 */
void errorMessage()
{
	/*Send error reading*/
	uint8_t Msg[] = "Incorrect message\r";
	CDC_Transmit_FS(Msg, myStrlen(Msg));
}

/**
 * @brief Sends actual global position to the USB
 */
void sendPosition()
{
	Position position = GetGlobalPostition();
	if(position.homed && !getRequestHomePos())
	{
		uint8_t Msg[64];
		snprintf((char *)Msg, 64,"%lu,%lu,%lu,%lu\r", position.x, position.y, position.z, position.fi);
		CDC_Transmit_FS(Msg, myStrlen(Msg));
	}
}

/**
 * @brief Will parse manual message and set variables, the messages types are X+ and similar or STOP, HOME, POSITION
 * @param str: String containing manual message
 */
void parseManualMessage(const char *str)
{
	// STOP message
	if(!strcmp(str, "STOP")) //strcmp returns zero if the strings match
	{
		manualState_t manual = getManualState();
		// Check if the message is relevant
		if(manual.isManualState)
		{
			Position pos = GetGlobalPostition();
			int counter;
			// Mutex on settings the the motors in case that the setting has not been finished
			if(osMutexWait(manualMotorSettingMutex, osWaitForever) == osOK)
			{
				switch(manual.manualAxis)
				{
				case axis_x:
					// For axis X, stop PWM timer 1, read timer 4 counter and calculate the new position
					HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
					htim1.Instance->ARR = 0;
					counter = __HAL_TIM_GetCounter(&htim4) * manual.manualDir;
					HAL_TIM_Base_Stop_IT(&htim4);
					__HAL_TIM_SET_COUNTER(&htim4, 0);
					SetGlobalPostition(pos.x + counter, -1, -1, -1);
					break;
				case axis_y:
					// For axis Y, stop PWM timer 2, read timer 9 counter and calculate the new position
					HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
					htim2.Instance->ARR = 0;
					counter = __HAL_TIM_GetCounter(&htim9) * manual.manualDir;
					HAL_TIM_Base_Stop_IT(&htim9);
					__HAL_TIM_SET_COUNTER(&htim9, 0);
					SetGlobalPostition(-1, pos.y + counter, -1, -1);
					break;
				case axis_z:
					// For axis Z, stop PWM timer 3, read timer 4 counter and calculate the new position
					HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
					htim3.Instance->ARR = 0;
					counter = __HAL_TIM_GetCounter(&htim4) * manual.manualDir;
					HAL_TIM_Base_Stop_IT(&htim4);
					__HAL_TIM_SET_COUNTER(&htim4, 0);
					SetGlobalPostition(-1, -1, pos.z + counter, -1);
					break;
				case axis_fi:
					// For axis FI, stop PWM timer 11, set position to the middle of range (horizontal position)
					HAL_TIM_PWM_Stop(&htim11, TIM_CHANNEL_1);
					htim11.Instance->ARR = 0;
					// Special axis - doesn't have home position
					SetGlobalPostition(-1, -1, -1, MACHINE_RANGE_FI/2);
					HAL_TIM_Base_Stop_IT(&htim9);
					__HAL_TIM_SET_COUNTER(&htim9, 0);
					break;
				default:
					setManualState(0, 0, axis_unspecified);
					return;
				}
				osMutexRelease(manualMotorSettingMutex);
			}
			// End the manual state
			setManualState(0, 0, axis_unspecified);
		}
		// Send the new gained position
		sendPosition();
		return;
	}
	// HOME message
	else if(!strcmp(str, "HOME"))
	{
		setRequestHomePos(1);
		return;
	}
	// Asking for position message
	else if(!strcmp(str, "POSITION"))
	{
		sendPosition();
		return;
	}

	int len = myStrlen((uint8_t *) str);
	// The length of the message is unknown
	if(len != 2)
	{
		errorMessage();
		return;
	}
	int dir = 0;
	axes_t axis = axis_unspecified;
	switch(str[0])
	{
	case 'X':
		axis = axis_x;
		break;
	case'Y':
		axis = axis_y;
		break;
	case 'Z':
		axis = axis_z;
		break;
	case 'F':
		axis = axis_fi;
		break;
	default:
		return;
	}
	switch(str[1])
	{
	case '+':
		dir = 1;
		break;
	case '-':
		dir = -1;
		break;
	default:
		return;
	}
	// Set information about manual state
	setManualState(1, dir, axis);
}
