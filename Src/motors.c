/**
  ******************************************************************************
  * @file           : motors.c
  * @brief          : File for setting timers for motor steps, setting
  * 				  its speed and counting, setting the direction of the
  * 				  movement.
  * 				  Contains initialization and de-initialization of the
  * 				  motors and their homing.
  ******************************************************************************
  * Created on: 20. 4. 2020
  * Author: novakha5
  ******************************************************************************
*/

#include "motors.h"
#include "init.h"
#include "parser.h"
#include "position.h"
#include "usbd_cdc_if.h"

manualState_t manualState;
osMutexId manualStateMutex;

osMutexId requestHomeMutex;
int requestHomePosition;

/**
 * @brief Enable motor drivers and initialize mutexes
 */
void initMotors()
{
	HAL_GPIO_WritePin(ENABLE_MOTORS_GPIO_Port, ENABLE_MOTORS_Pin, GPIO_PIN_SET);
	manualState.isManualState = 0;
	/*Mutex for manual State variable*/
	osMutexDef(manualStateMutex);
	manualStateMutex = osMutexCreate(osMutex(manualStateMutex));

	requestHomePosition = 0;
	/*Mutex for manual State variable*/
	osMutexDef(requestHomeMutex);
	requestHomeMutex = osMutexCreate(osMutex(requestHomeMutex));

	/*Mutex for manual settings - the settings can be slower then Stop coming by USB*/
	osMutexDef(manualMotorSettingMutex);
	manualMotorSettingMutex = osMutexCreate(osMutex(manualMotorSettingMutex));
}

/**
 * @brief Disable motor drivers
 */
void deinitMotors()
{
	HAL_GPIO_WritePin(ENABLE_MOTORS_GPIO_Port, ENABLE_MOTORS_Pin, GPIO_PIN_RESET);
}

/**
 * @brief Sets the variable requestHomePosition to given value
 * @param val: Value to which the variable should be set
 */
void setRequestHomePos(int val)
{
	if(osMutexWait(requestHomeMutex, osWaitForever) == osOK)
	{
		requestHomePosition = val;
		osMutexRelease(requestHomeMutex);
	}
}

/**
 * @brief Give value of requestHomePosition variable
 */
int getRequestHomePos()
{
	int ret = -1;
	if(osMutexWait(requestHomeMutex, osWaitForever) == osOK)
	{
		ret = requestHomePosition;
		osMutexRelease(requestHomeMutex);
	}
	return ret;
}

/**
 * @brief Will slowly run the motors to the 0,0,0,0 position (till the end sensors give the signal)
 */
void homeMotors()
{
	Position pos = GetGlobalPostition();
	/*if the home has been done - run as close as possible to the desired position*/
	if(pos.homed)
	{
		ClearAllSteps();
		uint8_t Msg[64];
		snprintf((char *)Msg, 64,"%lu,%lu,10,%lu\r", pos.x, pos.y, pos.fi);
		parseLine(Msg);
		while(AnyStepsSet())
		{
			controlMotors();
			osDelay(10);
		}
		uint f = 100*MICRO_STEPS_FI;
		snprintf((char *)Msg, 64,"10,10,10,%u\r", f);
		parseLine(Msg);
		while(AnyStepsSet())
		{
			controlMotors();
			osDelay(10);
		}
		ClearHomePosition();
	}
	/*Find out the status of the sensors*/
	GPIO_PinState x = HAL_GPIO_ReadPin(LIMIT_X_GPIO_Port, LIMIT_X_Pin);
	GPIO_PinState y = HAL_GPIO_ReadPin(LIMIT_Y_GPIO_Port, LIMIT_Y_Pin);
	GPIO_PinState z = HAL_GPIO_ReadPin(LIMIT_Z_GPIO_Port, LIMIT_Z_Pin);
	//GPIO_PinState fi = HAL_GPIO_ReadPin(LIMIT_FI_GPIO_Port, LIMIT_FI_Pin);
	setEndOfLine(axis_x, x);
	setEndOfLine(axis_y, y);
	setEndOfLine(axis_z, z);

	// Sensors are not installed
	setEndOfLine(axis_fi, 1);

	EndOfLine end = getEndOfLine();

	// The Z axis need to be on 0 position first so no crash will occure while moving X, Y
	if(end.z != 1)
		{
			SetSingleSteps(-MACHINE_RANGE_Z, axis_z);
			SetEnableDisableAxis(axis_z, 1);
		}
	if(end.fi != 1)
		{
			SetSingleSteps(-MACHINE_RANGE_FI, axis_fi);
			SetEnableDisableAxis(axis_fi, 1);
		}
	osDelay(1);
	while(end.z != 1 || end.fi != 1)
	{
		controlMotors();
		osDelay(10);
		end = getEndOfLine();
	}
	// home X and Y axis
	if(end.x != 1)
		{
			SetSingleSteps(-MACHINE_RANGE_X, axis_x);
			SetEnableDisableAxis(axis_x, 1);
		}
	if(end.y != 1)
		{
			SetSingleSteps(-MACHINE_RANGE_Y, axis_y);
			SetEnableDisableAxis(axis_y, 1);
		}
	while(end.x != 1 || end.y != 1)
	{
		controlMotors();
		osDelay(10);
		end = getEndOfLine();
	}
	/* Set global position to home*/
	InitGlobalPosition(1);
	/* Clear steps and end of line indicators */
	ClearAllSteps();
	clearEndOfLine();
	/* Sets the variable requestHomePosition to false*/
	setRequestHomePos(0);
	/* Send information about home position via USB */
	sendPosition();
}


/**
 * @brief Function getting outside input from PC, to specify position of the first component
 * Call only if manual position is active
 */
void manualPosition()
{
	manualState_t manual = getManualState();
	if(!manual.isManualState) return;

	GPIO_TypeDef* port = DIR_X_GPIO_Port;
	uint16_t pin = DIR_X_Pin;
	GPIO_PinState state = GPIO_PIN_RESET;

	if(manual.manualDir < 0) state = GPIO_PIN_RESET;
	else state = GPIO_PIN_SET;

	Position pos = GetGlobalPostition();
	int freeWay = 1;
	switch(manual.manualAxis)
	{
	  case axis_x:
		if((manual.manualDir < 0 && pos.x == 0) || (manual.manualDir > 0 && pos.x == MACHINE_RANGE_X))
		{
			freeWay = 0;
		}
		port = DIR_X_GPIO_Port;
		pin = DIR_X_Pin;
		break;
	  case axis_y:
		if((manual.manualDir < 0 && pos.y == 0) || (manual.manualDir > 0 && pos.y == MACHINE_RANGE_Y))
		{
			freeWay = 0;
		}
		port = DIR_Y_GPIO_Port;
		pin = DIR_Y_Pin;
		break;
	  case axis_z:
		if((manual.manualDir < 0 && pos.z == 0) || (manual.manualDir > 0 && pos.z == MACHINE_RANGE_Z))
		{
			freeWay = 0;
		}
		port = DIR_Z_GPIO_Port;
		pin = DIR_Z_Pin;
		// The Z axis has switched directions
		if(manual.manualDir < 0) state = GPIO_PIN_SET;
		else state = GPIO_PIN_RESET;
		break;
	  case axis_fi: // This axis has no boundaries to make it impossible to move forward
		port = DIR_FI_GPIO_Port;
		pin = DIR_FI_Pin;
		// The Fi axis has switched directions
		if(manual.manualDir < 0) state = GPIO_PIN_SET;
		else state = GPIO_PIN_RESET;
		break;
	  default:
		return;
	}
	if(freeWay == 0)
	{
		/*Send error reading*/
		uint8_t Msg[] = "The direction is not possible\r";
		CDC_Transmit_FS(Msg, myStrlen(Msg));
		setManualState(0, 0, axis_unspecified);
		return;
	}
	// Get current state in case of stop comming before the setting is done
	manual = getManualState();
	if(manual.isManualState)
	{
		if(osMutexWait(manualMotorSettingMutex, osWaitForever) == osOK)
		{
			HAL_GPIO_WritePin(port, pin, state);
			setSteps(manual.manualAxis, TIM_SIZE);
			moveMotor(manual.manualAxis, 498);
			osMutexRelease(manualMotorSettingMutex);
		}
	}
}

/**
 * @brief Will set given values to manualState structure, if valid
 * @param state: 1 for manual state on, otherwise 0
 * @param dir: 1 for + direction, -1 for - direction
 * @param axis: axes_t value containing information which axis should move
 */
void setManualState(int state, int dir, axes_t axis)
{
	if(osMutexWait(manualStateMutex, osWaitForever) == osOK)
	{
		if(state == 0 || state == 1)
		{
			manualState.isManualState = state;
		}
		if(dir == 1 || dir == -1)
		{
			manualState.manualDir = dir;
		}
		if(axis != axis_unspecified)
		{
			manualState.manualAxis = axis;
		}
		osMutexRelease(manualStateMutex);
	}
}

/**
 * @brief Give manualState values
 * @return manualState_t structure containing information about state, direction and axis
 */
manualState_t getManualState()
{
	manualState_t ret;
	ret.isManualState = -1;
	ret.manualAxis = axis_unspecified;
	ret.manualDir = 0;
	if(osMutexWait(manualStateMutex, osWaitForever) == osOK)
	{
		ret = manualState;
		osMutexRelease(manualStateMutex);
	}
	return ret;
}


/**
 * @brief Will look if there are any steps remaining and sets the motors accordingly
 */
void controlMotors()
{
	if(getManualState().isManualState)
	{
		manualPosition();
		while(getManualState().isManualState)
		{osDelay(10);}
		return;
	}
	if(!AnyStepsSet()) return;
	uint32_t counterX = 0;
	uint32_t counterY = 0;
	uint32_t counterZ = 0;
	uint32_t counterFi = 0;
	int period;

	Steps_t steps = GetWaitingSteps();
	Steps_remain_t remain = GetRemainingSteps();
	if(steps.steps_x != 0)
	{
		counterX = __HAL_TIM_GetCounter(&htim4);
		if(abs(steps.steps_x) > TIM_SIZE && htim1.Instance->ARR != 0) //Make sure the motors are running (so the remaining steps has been set)
		{
			counterX = counterX + CalculateCounterWithOverrun(abs(steps.steps_x), remain.steps_x);
		}
		if(steps.enable_x)
		{
			SetEnableDisableAxis(axis_x, 0);
			/* Set the direction pin */
			if(steps.steps_x < 0)
			{
				HAL_GPIO_WritePin(DIR_X_GPIO_Port, DIR_X_Pin, GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(DIR_X_GPIO_Port, DIR_X_Pin, GPIO_PIN_SET);
			}
			/* Set steps counter */
			setSteps(0, abs(steps.steps_x));
			/* PWM settings and start */
			moveMotor(0, calculatePeriod(counterX, abs(steps.steps_x)));
		}
		else
		{
			/* Update of the period */
			period = calculatePeriod(counterX, abs(steps.steps_x))/MICRO_STEPS;
			if(period != htim1.Instance->ARR){
				htim1.Instance->ARR = period;
			}
		}
	}
	if(steps.steps_y != 0)
	{
		counterY = __HAL_TIM_GetCounter(&htim9);
		if(abs(steps.steps_y) > TIM_SIZE && htim2.Instance->ARR != 0)
		{
			counterY = counterY + CalculateCounterWithOverrun(abs(steps.steps_y), remain.steps_y);
		}
		if(steps.enable_y)
		{
			SetEnableDisableAxis(axis_y, 0);
			if(steps.steps_y < 0)
			{
				HAL_GPIO_WritePin(DIR_Y_GPIO_Port, DIR_Y_Pin, GPIO_PIN_RESET);
			}
			else
			{
				HAL_GPIO_WritePin(DIR_Y_GPIO_Port, DIR_Y_Pin, GPIO_PIN_SET);
			}
			setSteps(1, abs(steps.steps_y));
			moveMotor(1, calculatePeriod(counterY, abs(steps.steps_y)));
		} else
		{
			period = calculatePeriod(counterY, abs(steps.steps_y))/MICRO_STEPS;
			if(period != htim2.Instance->ARR){
				htim2.Instance->ARR = period;
			}
		}
	}
	/*Z can't run while the X or Y is running - it could end up in crash to the buttons*/
	if(steps.steps_z != 0 && steps.steps_x == 0 && steps.steps_y == 0)
	{
		counterZ = __HAL_TIM_GetCounter(&htim4);
		if(abs(steps.steps_z) > TIM_SIZE && htim3.Instance->ARR != 0)
		{
			counterZ = counterZ + CalculateCounterWithOverrun(abs(steps.steps_z), remain.steps_z);
		}
		if(steps.enable_z)
		{
			SetEnableDisableAxis(axis_z, 0);
			if(steps.steps_z < 0)
			{
				HAL_GPIO_WritePin(DIR_Z_GPIO_Port, DIR_Z_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(DIR_Z_GPIO_Port, DIR_Z_Pin, GPIO_PIN_RESET);
			}
			setSteps(2, abs(steps.steps_z));
			moveMotor(2, calculatePeriod(counterZ, abs(steps.steps_z)));
		}
		else
		{
			period = calculatePeriod(counterZ, abs(steps.steps_z))/MICRO_STEPS;
			if(period != htim3.Instance->ARR){
				htim3.Instance->ARR = period;
			}
		}
	}
	/* Fi can't run while the Y is running - use same counter*/
	if(steps.steps_fi != 0 && steps.steps_x == 0 && steps.steps_y == 0 && steps.steps_z == 0)
	{
		counterFi = __HAL_TIM_GetCounter(&htim9);
		if(abs(steps.steps_fi) > TIM_SIZE && htim11.Instance->ARR != 0)
		{
			counterFi = counterFi + CalculateCounterWithOverrun(abs(steps.steps_fi), remain.steps_fi);
		}
		if(steps.enable_fi)
		{
			SetEnableDisableAxis(axis_fi, 0);
			if(steps.steps_fi < 0)
			{
				HAL_GPIO_WritePin(DIR_FI_GPIO_Port, DIR_FI_Pin, GPIO_PIN_SET);
			}
			else
			{
				HAL_GPIO_WritePin(DIR_FI_GPIO_Port, DIR_FI_Pin, GPIO_PIN_RESET);
			}
			setSteps(3, abs(steps.steps_fi));
			moveMotor(3, calculatePeriod(counterFi, abs(steps.steps_fi)));
		}
		else
		{
			period = calculatePeriod(counterFi, abs(steps.steps_fi))/MICRO_STEPS_FI;
			if(period != htim11.Instance->ARR){
				htim11.Instance->ARR = period;
			}
		}
	}
}

#define changingSteps 	(400.0*MICRO_STEPS) /**< How many steps should be slowed down before the max speed*/
#define maxPeriod		1024 /**< Longest period - for the slowest steps */
#define minPeriod		(10*MICRO_STEPS) /**< Period for the maximum speed */
#define maxFreq			1.0/minPeriod /**< Frequency for low speed */
#define minFreq			1.0/maxPeriod /**< Frequency for maximum speed */

/**
 * @brief Will calculate period for PWM
 * @param stepsfromBegining: How much steps were done from begining
 * @param stepsMax: For how much steps the motor is running
 * @retval Period to set
 * */
/* The maximum speed should be on period minPeriod (counts from 0) */
/* Calculated to 125 kHz on base in initialization - Prescaler */
int calculatePeriod(int stepsfromBegining, int stepsMax)
{
	if(!isHomed())
	{
		return 298;
	}
	int period;
	float x;
	long stepsRemaining = stepsMax - stepsfromBegining;
	if(stepsRemaining >= changingSteps || (stepsRemaining > stepsfromBegining))
	{
		/*Slow down start of the ramp*/
		if(stepsfromBegining <= 10)
		{
			/*Needs to be shifted since it counts from 0*/
			period = maxPeriod-1;
		}
		else if(stepsfromBegining < changingSteps)
		{
			x = minFreq + ((stepsfromBegining-10)*((maxFreq-minFreq)/changingSteps));
			period = (1/x)-1;
		}
		else
		{
			period = minPeriod-1;
		}
	}
	else
	{
		/*Slow down end of the ramp*/
		if(stepsRemaining <= 10)
		{
			period = maxPeriod-1;
		}
		else
		{
			x = minFreq + ((stepsRemaining-10)*((maxFreq-minFreq)/changingSteps));
			period = (1/x)-1;
		}
	}
	return period;
}

/**
 * @brief Activate the PWM output for  given motor for given number of steps
 * @param motor: which motor should be moved 0 for motors x, 1 for motor y, 2 for motor z and 3 for motor fi
 * @param period: how quick should motor move - period of the PWM timer
 */
void moveMotor(int motor, int period)
{
	switch(motor)
	{
	  //do the settings of number of steps and speed
	  case 0: //motor X
		  setPWM(htim1, TIM_CHANNEL_1, period/MICRO_STEPS);
		  break;
	  case 1: //motor Y
		  setPWM(htim2, TIM_CHANNEL_1, period/MICRO_STEPS);
		  break;
	  case 2://motor Z
		  setPWM(htim3, TIM_CHANNEL_1, period/MICRO_STEPS);
		  break;
	  case 3: //motor Fi
		  setPWM(htim11, TIM_CHANNEL_1, period/MICRO_STEPS_FI);
		  break;
	}
}

/**
 * @brief Sets the PWM timer settings and starts it
 * @param timer: For which timer should be the PWM set
 * @param channel: For witch channel of the timer
 * @param period: How long the period of the PWM should be
 * */
void setPWM(TIM_HandleTypeDef timer, uint32_t channel, uint16_t period)
{
	TIM_MasterConfigTypeDef sMasterConfig = {0};
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_OC1REF;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_ENABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&timer, &sMasterConfig) != HAL_OK)
	{
	  Error_Handler();
	}
	// set the period duration
	timer.Init.Period = period;
	timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	// re-initialize with new period value
	HAL_TIM_PWM_Init(&timer);

	TIM_OC_InitTypeDef sConfigOC;
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	// set the pulse duration
	sConfigOC.Pulse = 9;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&timer, &sConfigOC, channel);
	// start PWM generation
	HAL_TIM_PWM_Start(&timer, channel);
}

/**
 * @brief Sets the slave timer to count steps for given motor
 * @param motor: which motor should be used 0 for motors x, 1 for motor y, 2 for motor z and 3 for motor fi
 * @param numOfSteps: how many steps should be done
 */
void setSteps(int motor, int numOfSteps)
{
	int32_t remaining = 0;
	if(numOfSteps > TIM_SIZE)
	{
		remaining = numOfSteps-TIM_SIZE;
		numOfSteps = TIM_SIZE;
	}
	if(numOfSteps > 1)	numOfSteps--; //It counts to next cycle, so it will do one more step then required
	TIM_SlaveConfigTypeDef sSlaveConfig = {0};
	switch(motor)
	{
	case 0: //motor X
		// Set TIM4 slave mode from ITR0 to count to stepsRemaining
		sSlaveConfig.SlaveMode = TIM_SLAVEMODE_EXTERNAL1;
		sSlaveConfig.InputTrigger = TIM_TS_ITR0;
		if (HAL_TIM_SlaveConfigSynchro(&htim4, &sSlaveConfig) != HAL_OK)
		{
			Error_Handler();
		}
		htim4.Init.Period = numOfSteps;
		if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
		{
		  Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&htim4, 0);
		__HAL_TIM_CLEAR_IT(&htim4 ,TIM_IT_CC1);
		if(HAL_TIM_Base_Start_IT(&htim4) != HAL_OK)
		{
			Error_Handler();
		}
		SetRemaining(remaining, axis_x);
		break;
    case 1: //motor Y
    	// Set TIM9 slave mode from ITR0 to count to stepsRemaining
		sSlaveConfig.SlaveMode = TIM_SLAVEMODE_EXTERNAL1;
		sSlaveConfig.InputTrigger = TIM_TS_ITR0;
		if (HAL_TIM_SlaveConfigSynchro(&htim9, &sSlaveConfig) != HAL_OK)
		{
			Error_Handler();
		}
		htim9.Init.Period = numOfSteps;
		if (HAL_TIM_Base_Init(&htim9) != HAL_OK)
		{
		  Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&htim9, 0);
		__HAL_TIM_CLEAR_IT(&htim9 ,TIM_IT_CC1);
		if(HAL_TIM_Base_Start_IT(&htim9) != HAL_OK)
		{
			Error_Handler();
		}
		SetRemaining(remaining, axis_y);
    	break;
    case 2: //motor Z
    	// Set TIM4 slave mode from ITR2 to count to stepsRemaining
		sSlaveConfig.SlaveMode = TIM_SLAVEMODE_EXTERNAL1;
		sSlaveConfig.InputTrigger = TIM_TS_ITR2;
		if (HAL_TIM_SlaveConfigSynchro(&htim4, &sSlaveConfig) != HAL_OK)
		{
			Error_Handler();
		}
		htim4.Init.Period = numOfSteps;
		if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
		{
		  Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&htim4, 0);
		__HAL_TIM_CLEAR_IT(&htim4 ,TIM_IT_CC1);
		if(HAL_TIM_Base_Start_IT(&htim4) != HAL_OK)
		{
			Error_Handler();
		}
		SetRemaining(remaining, axis_z);
		break;
    case 3: //motor Fi
    	// Set TIM9 slave mode from ITR3 to count to stepsRemaining
		sSlaveConfig.SlaveMode = TIM_SLAVEMODE_EXTERNAL1;
		sSlaveConfig.InputTrigger = TIM_TS_ITR3;
		if (HAL_TIM_SlaveConfigSynchro(&htim9, &sSlaveConfig) != HAL_OK)
		{
			Error_Handler();
		}
		htim9.Init.Period = numOfSteps;
		if (HAL_TIM_Base_Init(&htim9) != HAL_OK)
		{
		  Error_Handler();
		}
		__HAL_TIM_SET_COUNTER(&htim9, 0);
		__HAL_TIM_CLEAR_IT(&htim9 ,TIM_IT_CC1);
		if(HAL_TIM_Base_Start_IT(&htim9) != HAL_OK)
		{
			Error_Handler();
		}
		SetRemaining(remaining, axis_fi);
    	break;
	}
}

/**
 * @brief Will check if there are any steps which has been run before this counter settings
 * @param steps: How many steps should be done together
 * @param remaining: How many steps has not been placed to counter yet
 * @retval The amout of steps which has been run before this counter settings
 * */
int32_t CalculateCounterWithOverrun(int32_t steps, int32_t remaining)
{
	// The remaining is missing the part where it is now, but it has not been finished yet
	int remainOverrun = (remaining/TIM_SIZE) + 1;
	if(remaining == 0)
	{	// In the last part, there will be no more overriding
		remainOverrun = 0;
	}
	int overrun = (steps/TIM_SIZE) - remainOverrun;
	return overrun*TIM_SIZE;
}

