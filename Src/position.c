/**
  ******************************************************************************
  * @file           : position.c
  * @brief          : File for storing, rewriting and reading global position
  * 				  and the state of the end switches.
  ******************************************************************************
  * Created on: 11. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/

#include "position.h"
#include "stm32f4xx_hal.h"
#include "main.h"
#include "init.h"

Position globalPos;
EndOfLine switchIndication;

/**
 * @brief 	Initialize globalPos to [0 0 0 0] state
 */
void InitGlobalPosition(uint8_t homed)
{
	if(osMutexWait(positionMutex, osWaitForever) == osOK)
	{
		globalPos.x = 0;
		globalPos.y = 0;
		globalPos.z= 0;
		globalPos.fi = 100*MICRO_STEPS_FI; // Upper position
		globalPos.homed = homed;
		osMutexRelease(positionMutex);
	}

	if(osMutexWait(sensorMutex, osWaitForever) == osOK)
	{
		switchIndication.x = 0;
		switchIndication.y = 0;
		switchIndication.z = 0;
		switchIndication.fi = 0;
		osMutexRelease(sensorMutex);
	}
}


/**
 * @brief 	Reads globalPos value
 * @retval	Position structure filled with current globalPos values
 */
Position GetGlobalPostition(void)
{
	Position ret;
	if(osMutexWait(positionMutex, osWaitForever) == osOK)
	{
		ret.x = globalPos.x;
		ret.y = globalPos.y;
		ret.z = globalPos.z;
		ret.fi = globalPos.fi;
		ret.homed = globalPos.homed;
		osMutexRelease(positionMutex);
	}
	return ret;
}

void ClearHomePosition()
{
	if(osMutexWait(positionMutex, osWaitForever) == osOK)
	{
		globalPos.homed = 0;
		osMutexRelease(positionMutex);
	}
}

/**
 * @brief 	Check if the CNC was homed
 * @retval	1 if homed, otherwise 0
 */
uint8_t isHomed(void)
{
	return globalPos.homed;
}

/**
 * @brief	Sets the new position to globalPos structure
 * @param x: value to set for position of axis x, -1 to leave previous value
 * @param y: value to set for position of axis y, -1 to leave previous value
 * @param z: value to set for position of axis z, -1 to leave previous value
 * @param fi: value to set for position of axis fi, -1 to leave previous value
 */
void SetGlobalPostition(int32_t x, int32_t y, int32_t z, int32_t fi)
{
	if(osMutexWait(positionMutex, osWaitForever) == osOK)
	{
		if(x >= 0) /*Check if position of axis x should be changed*/
		{
			globalPos.x = x;
		}
		if(y >= 0) /*Check if position of axis y should be changed*/
		{
			globalPos.y = y;
		}
		if(z >= 0) /*Check if position of axis z should be changed*/
		{
			globalPos.z = z;
		}
		if(fi >= 0) /*Check if position of axis fi should be changed*/
		{
			globalPos.fi = fi;
		}
		osMutexRelease(positionMutex);
	}
}

/**
 * @brief 	Reads switchIndication value
 * @retval	Position structure filled with current switchIndication values
 */
EndOfLine getEndOfLine()
{
	EndOfLine ret;
	if(osMutexWait(sensorMutex, osWaitForever) == osOK)
	{
		ret.x = switchIndication.x;
		ret.y = switchIndication.y;
		ret.z = switchIndication.z;
		ret.fi = switchIndication.fi;
		osMutexRelease(sensorMutex);
	}
	return ret;
}

/**
 * @brief	Sets the new position to switchIndication structure
 * @param axis: Which axis end sensors send the indication, or which we want clear
 * @param set: Which end of axis, 0 for clearing, 1 for home position, 2 for end position
 */
void setEndOfLine(axes_t axis, uint8_t set)
{
	if(osMutexWait(sensorMutex, osWaitForever) == osOK)
	{
		switch(axis)
		{
		  case axis_x:
			switchIndication.x = set;
			break;
		  case axis_y:
			switchIndication.y = set;
			break;
		  case axis_z:
			switchIndication.z = set;
			break;
		  case axis_fi:
			switchIndication.fi = set;
			break;
		  default:
			  break;
		}
		osMutexRelease(sensorMutex);
	}
}

/**
 * @brief Will clear all the values in switchIndication structure
 */
void clearEndOfLine()
{
	setEndOfLine(axis_x, 0);
	setEndOfLine(axis_y, 0);
	setEndOfLine(axis_z, 0);
	setEndOfLine(axis_fi, 0);
}
