/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
#include "FreeRTOS.h"
#include "task.h"
#include "stepsToRun.h"
#include "stm32f4xx_hal_tim.h"

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim9;
extern TIM_HandleTypeDef htim10;

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{

}

static volatile unsigned int _Continue;
/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
	_Continue = 0u;
	while (_Continue == 0u);
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  while (1)
  {
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  while (1)
  {
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  while (1)
  {
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles TIM1 break interrupt and TIM9 global interrupt.
  */
void TIM1_BRK_TIM9_IRQHandler(void)
{
  if (__HAL_TIM_GET_FLAG(&htim9, TIM_FLAG_CC1) != RESET)
  {
	  Steps_t steps = GetWaitingSteps();
	  Steps_remain_t remain = GetRemainingSteps();
	  Position pos = GetGlobalPostition();
	  if(steps.steps_y == 0)
	  {
		  // for timer fi
		  if(remain.steps_fi != 0)
		  {
			  int32_t steps = remain.steps_fi;
			  if(remain.steps_fi > TIM_SIZE)
			  {
				  steps = TIM_SIZE;
				  SetRemaining(remain.steps_fi - TIM_SIZE, axis_fi);
			  }
			  else
			  {
				  SetRemaining(0, axis_fi);
			  }
			  if(steps > 1)	steps--; //It counts from 0, so it will do one more step then required
			  htim9.Instance->ARR = steps;
		  }
		  else
		  {
			  HAL_TIM_PWM_Stop(&htim11, TIM_CHANNEL_1);
			  htim11.Instance->ARR = 0;
			  if(pos.homed)
			  {
				  SetGlobalPostition(-1, -1, -1, pos.fi + steps.steps_fi);
			  }
			  SetSingleSteps(0, axis_fi);
			  HAL_TIM_Base_Stop_IT(&htim9);
			  __HAL_TIM_SET_COUNTER(&htim9, 0);
		  }
	  }
	  else
	  {
		  //for timer y
		  if(remain.steps_y > 0)
		  {
			  int32_t steps = remain.steps_y;
			  if(remain.steps_y > TIM_SIZE)
			  {
				  steps = TIM_SIZE;
				  SetRemaining(remain.steps_y - TIM_SIZE, axis_y);
			  }
			  else
			  {
				  SetRemaining(0, axis_y);
			  }
			  if(steps > 1)	steps--; //It counts from 0, so it will do one more step then required
			  htim9.Instance->ARR = steps;
		  }
		  else
		  {
			  HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
			  htim2.Instance->ARR = 0;
			  if(pos.homed)
			  {
				  SetGlobalPostition(-1, pos.y + steps.steps_y, -1, -1);
			  }
			  SetSingleSteps(0, axis_y);
			  HAL_TIM_Base_Stop_IT(&htim9);
			  __HAL_TIM_SET_COUNTER(&htim9, 0);
		  }
	  }
  }
  HAL_TIM_IRQHandler(&htim1);
  HAL_TIM_IRQHandler(&htim9);
}

/**
  * @brief This function handles TIM1 update interrupt and TIM10 global interrupt.
  */
void TIM1_UP_TIM10_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim1);
  HAL_TIM_IRQHandler(&htim10);
}

/**
  * @brief This function handles TIM5 global interrupt.
  */
void TIM4_IRQHandler(void)
{
  if (__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC1) != RESET)
  {
	  Steps_t steps = GetWaitingSteps();
	  Steps_remain_t remain = GetRemainingSteps();
	  Position pos = GetGlobalPostition();
	  if(steps.steps_x == 0)
	  {
		  // for timer z
		  if(remain.steps_z != 0)
		  {
			  int32_t steps = remain.steps_z;
			  if(remain.steps_z > TIM_SIZE)
			  {
				  steps = TIM_SIZE;
				  SetRemaining(remain.steps_z - TIM_SIZE, axis_z);
			  }
			  else
			  {
				  SetRemaining(0, axis_z);
			  }
			  if(steps > 1)	steps--; //It counts from 0, so it will do one more step then required
			  htim4.Instance->ARR = steps;
		  }
		  else
		  {
			  HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			  htim3.Instance->ARR = 0;
			  if(pos.homed)
			  {
				  SetGlobalPostition(-1, -1, pos.z + steps.steps_z, -1);
			  }
			  SetSingleSteps(0, axis_z);
			  HAL_TIM_Base_Stop_IT(&htim4);
			  __HAL_TIM_SET_COUNTER(&htim4, 0);
		  }
	  }
	  else
	  {
		  //for timer x
		  if(remain.steps_x != 0)
		  {
			  int32_t steps = remain.steps_x;
			  if(remain.steps_x > TIM_SIZE)
			  {
				  steps = TIM_SIZE;
				  SetRemaining(remain.steps_x - TIM_SIZE, axis_x);
			  }
			  else
			  {
				  SetRemaining(0, axis_x);
			  }
			  if(steps > 1)	steps--; //It counts from 0, so it will do one more step then required
			  htim4.Instance->ARR = steps;
		  }
		  else
		  {
			  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
			  htim1.Instance->ARR = 0;
			  if(pos.homed)
			  {
				  SetGlobalPostition(pos.x + steps.steps_x, -1, -1, -1);
			  }
			  SetSingleSteps(0, axis_x);
			  HAL_TIM_Base_Stop_IT(&htim4);
			  __HAL_TIM_SET_COUNTER(&htim4, 0);
		  }
	  }
  }
  HAL_TIM_IRQHandler(&htim4);
}

/**
  * @brief This function handles USB On The Go FS global interrupt.
  */
void OTG_FS_IRQHandler(void)
{
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}

/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
}

/**
  * @brief This function handles EXTI line[15:10] interrupts.
  */
void EXTI15_10_IRQHandler(void)
{
  if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_10) != RESET) //LIMIT_Y
  {
	  HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
	  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
  }
  else if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_11) != RESET) //LIMIT_Z
  {
	  HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
	  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
  }
  else if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_12) != RESET) //LIMIT_FI
  {
	  HAL_TIM_PWM_Stop(&htim11, TIM_CHANNEL_1);
	  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
  }
  else if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_15) != RESET) //LIMIT_X
  {
	  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
	  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
  }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
