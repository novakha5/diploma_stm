/**
  ******************************************************************************
  * @file           : init.h
  * @brief          : Initialization functions headers
  ******************************************************************************
  * Created on: 9. 4. 2020
  * Author: novakha5
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#ifndef INIT_H_
#define INIT_H_

#include "main.h"
#include "cmsis_os.h"
#include "usb_device.h"

#define USB_QUEUE_SIZE 1024

#define MICRO_STEPS			8	/**< Microstep selected on the driver maximum 8, since the int32 usage */
#define MICRO_STEPS_FI		2	/**< Microstep selection on smaller type driver */
/*Set maximum steps from position [0 0 0 0] on axes*/
#define MACHINE_RANGE_X 	(21000*MICRO_STEPS) /**< Sets how many steps from home position can be run on CNC machine in X axis */
#define MACHINE_RANGE_Y 	(39000*MICRO_STEPS) /**< Sets how many steps from home position can be run on CNC machine in Y axis */
#define MACHINE_RANGE_Z 	(4940*MICRO_STEPS) /**< Sets how many steps from home position can be run on CNC machine in Z axis */
#define MACHINE_RANGE_FI 	(200*MICRO_STEPS_FI) /**< Sets how many steps from home position can be run on CNC machine in FI axis */

#define TIM_SIZE 65536 /**< Timer size for counters, it is 16-bit timer */

extern IWDG_HandleTypeDef hiwdg;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim9;
extern TIM_HandleTypeDef htim11;

extern QueueHandle_t USBQueue;

/**
 * @brief	Call all needed configuration functions
 */
void System_init(void);

/**
  * @brief System Clock Configuration
  */
void SystemClock_Config(void);

/**
  * @brief GPIO Initialization Function
  */
void MX_GPIO_Init(void);

/**
  * @brief IWDG Initialization Function
  */
void MX_IWDG_Init(void);

/**
  * @brief TIM1 Initialization Function
  */
void MX_TIM1_Init(void);

/**
  * @brief TIM2 Initialization Function
  */
void MX_TIM2_Init(void);

/**
  * @brief TIM3 Initialization Function
  */
void MX_TIM3_Init(void);

/**
  * @brief TIM4 Initialization Function
  */
void MX_TIM4_Init(void);

/**
  * @brief USART6 Initialization Function
  */
void MX_USART6_UART_Init(void);

/**
  * @brief TIM9 Initialization Function
  */
void MX_TIM9_Init(void);

/**
  * @brief TIM11 Initialization Function
  */
void MX_TIM11_Init(void);

#endif /* INIT_H_ */
