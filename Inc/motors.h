/**
  ******************************************************************************
  * @file           : motors.h
  * @brief          : Header file for setting timers for motor steps, setting
  * 				  its speed and counting, setting the direction of the
  * 				  movement.
  * 				  Contains initialization and de-initialization of the
  * 				  motors and their homing.
  ******************************************************************************
  * Created on: 20. 4. 2020
  * Author: novakha5
  ******************************************************************************
*/

#ifndef MOTORTIMERS_H_
#define MOTORTIMERS_H_

#include "main.h"
#include "stepsToRun.h"

osMutexId manualMotorSettingMutex;

typedef struct
{
	int isManualState;
	int manualDir;
	axes_t manualAxis;
}manualState_t;

/**
 * @brief Enable motor drivers
 */
void initMotors();

/**
 * @brief Disable motor drivers
 */
void deinitMotors();

/**
 * @brief Sets the variable requestHomePosition to given value
 * @param val: Value to which the variable should be set
 */
void setRequestHomePos(int val);

/**
 * @brief Give value of requestHomePosition variable
 */
int getRequestHomePos();

/**
 * @brief Will slowly run the motors to the 0,0,0,0 position (till the end sensors give the signal)
 */
void homeMotors();

/**
 * @brief Function getting outside input from PC, to specify position of the first component
 * Call only if manual position is active
 */
void manualPosition();

/**
 * @brief Will set given values to manualState structure, if valid
 * @param state: 1 for manual state on, otherwise 0
 * @param dir: 1 for + direction, -1 for - direction
 * @param axis: axes_t value containing information which axis should move
 */
void setManualState(int state, int dir, axes_t axis);

/**
 * @brief Give manualState values
 * @return manualState_t structure containing information about state, direction and axis
 */
manualState_t getManualState();

/**
 * @brief Will look if there are any steps remaining and sets the motors accordingly
 */
void controlMotors();

/**
 * @brief Will calculate period for PWM
 * @param stepsfromBegining: How much steps were done from begining
 * @param stepsMax: For how much steps the motor is running
 * @retval Period to set
 * */
int calculatePeriod(int stepsfromBegining, int stepsMax);

/**
 * @brief Activate the PWM output for  given motor for given number of steps
 * @param motor: which motor should be moved 0 for motors x, 1 for motor y, 2 for motor z and 3 for motor fi
 * @param period: how quick should motor move - period of the PWM timer
 */
void moveMotor(int motor, int period);

/**
 * @brief Sets the PWM timer settings and starts it
 * @param timer: For which timer should be the PWM set
 * @param channel: For witch channel of the timer
 * @param period: How long the period of the PWM should be
 * */
void setPWM(TIM_HandleTypeDef timer, uint32_t channel, uint16_t period);

/**
 * @brief Sets the slave timer to count steps for given motor
 * @param motor: which motor should be used 0 for motors x, 1 for motor y, 2 for motor z and 3 for motor fi
 * @param numOfSteps: how many steps should be done
 */
void setSteps(int motor, int numOfSteps);

/**
 * @brief Will check if there are any steps which has been run before this counter settings
 * @param steps: How many steps should be done together
 * @param remaining: How many steps has not been placed to counter yet
 * @retval The amout of steps which has been run before this counter settings
 * */
int32_t CalculateCounterWithOverrun(int32_t steps, int32_t remaining);

#endif /* MOTORTIMERS_H_ */
