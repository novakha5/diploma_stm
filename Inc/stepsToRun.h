/**
  ******************************************************************************
  * @file           : stepsToRun.h
  * @brief          : Header file for initialize, store, change and read the
  * 				  steps which needs to be run.
  ******************************************************************************
  * Created on: 23. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/

#ifndef STEPSTORUN_H_
#define STEPSTORUN_H_

#include <stdint.h>
#include "position.h"
#include "init.h"

extern volatile osMutexId stepsMutex;
extern volatile osMutexId remainingMutex;

/** A structure type
 * Structure for storing the steps still needing to run for every axis
 */
typedef struct
{
	int32_t steps_x; /**< Steps needed to be run for x axis*/
	int8_t enable_x;
	int32_t steps_y; /**< Steps needed to be run for y axis*/
	int8_t enable_y;
	int32_t steps_z; /**< Steps needed to be run for z axis*/
	int8_t enable_z;
	int32_t steps_fi; /**< Steps needed to be run for fi axis*/
	int8_t enable_fi;
} Steps_t;

/** A structure type
 * Structure for storing the remaining steps still needing to run for every axis
 */
typedef struct
{
	int32_t steps_x; /**< Steps needed to be run for x axis*/
	int32_t steps_y; /**< Steps needed to be run for y axis*/
	int32_t steps_z; /**< Steps needed to be run for z axis*/
	int32_t steps_fi; /**< Steps needed to be run for fi axis*/
} Steps_remain_t;


/** An enum type
 * Enum of return values while setting steps
 */
typedef enum
{
	stepsOK 				= 0, /**< Return value indicating that there is no error */
	stepsStillRunning 		= 1, /**< Return value indicating that new values cannot be set since the last one has not been finished */
	stepsOutOfBoundaries 	= 2, /**< Return value indicating that the values are trying to set steps out of the CNC machine boundaries */
	stepsError				= 3 /**< Return value indicating any other error which occurred */
} stepsSucces_t;

/**
 * @brief Set steps and remaining steps value to 0
 * */
void InitSteps();

/**
 * @brief Tells if there are remaining steps
 * @retval 1 if there are any steps remaining, otherwise 0
 * */
int AnyStepsSet();

/**
 * @brief Give structure with steps remaining
 * @retval Structure with not finished steps
 * */
Steps_t GetWaitingSteps();

/**
 * @brief Sets steps to run for all axes if there are no running steps
 * @param x: Steps for axis x
 * @param y: Steps for axis y
 * @param z: Steps for axis z
 * @param fi: Steps for axis fi
 * @retval stepsOK, if the steps has been set,
 * stepsStillRunning if there are any remaining and the new one are not all zero,
 * stepsOutOfBounderies if the steps will lead to place out of the CNC
 * */
stepsSucces_t SetAllSteps(int32_t x, int32_t y, int32_t z, int32_t fi);

/**
 * @brief Set all values of StepsToRun to 0
 * */
void ClearAllSteps();

/**
 * @brief Set enable value in StepsToRun to state for given axis
 * @param axis: For which axis should be steps enabled/disabled
 * @param state: 1 for enable, 0 for disable
 * @return stepsOK, if enabled, stepsError if the axis or state is invalid
 * */
stepsSucces_t SetEnableDisableAxis(axes_t axis, uint8_t state);

/**
 * @brief Sets steps for one axis only
 * @param steps: number of steps to set
 * @param axis: for which axis the steps should be set
 * @retval stepsOK if the steps has been set,
 * stepsOutOfBounderies if the steps will lead to place out of the CNC
 * */
stepsSucces_t SetSingleSteps(int32_t steps, axes_t axis);

/**
 * @brief Set remaining steps for one axis
 * @param remaining: number of remaining steps to set
 * @param axis:  for which axis the remaining steps should be set
 * @retval stepsOK, if the steps has been set, otherwise stepsError
 * */
stepsSucces_t SetRemaining(int32_t remaining, axes_t axis);

/**
 * @brief Get structure with steps remaining to place to the counter
 * @retval Structure with remaining steps
 */
Steps_remain_t GetRemainingSteps();

#endif /* STEPSTORUN_H_ */
