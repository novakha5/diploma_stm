/**
  ******************************************************************************
  * @file           : position.h
  * @brief          : Header file for storing, rewriting and reading global
  * 				  position and the state of the end switches.
  ******************************************************************************
  * Created on: 11. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/

#ifndef POSITION_H_
#define POSITION_H_

#include "cmsis_os.h"

extern volatile osMutexId positionMutex;
extern volatile osMutexId sensorMutex;


/** A structure type
 * Structure to store position of the CNC
 */
typedef struct
{
	uint32_t x; /**< Position of x axis */
	uint32_t y; /**< Position of y axis */
	uint32_t z; /**< Position of z axis */
	uint32_t fi; /**< Position of fi axis */
	uint8_t homed; /**< Information if the CNC has been homed and therefore if the position is valid */
} Position;

/** A structure type
 * Structure to store information from CNC end switches
 */
typedef struct
{
	uint8_t x; /**< Information about x axis switches */
	uint8_t y; /**< Information about y axis switches */
	uint8_t z; /**< Information about z axis switches */
	uint8_t fi; /**< Information about fi axis switches */
} EndOfLine;

/** An enum type
 * Enum for indicating which axis should be set
 */
typedef enum
{
	axis_x = 0, /**< Axis x */
	axis_y = 1, /**< Axis y */
	axis_z = 2, /**< Axis z */
	axis_fi = 3, /**< Axis fi */
	axis_unspecified = -1
} axes_t;

/**
 * @brief 	Initialize globalPos to [0 0 0 0] state
 */
void InitGlobalPosition(uint8_t homed);

/**
 * @brief 	Reads globalPos value
 * @retval	Position structure filled with current globalPos values
 */
Position GetGlobalPostition(void);

void ClearHomePosition();

/**
 * @brief 	Check if the CNC was homed
 * @retval	1 if homed, otherwise 0
 */
uint8_t isHomed(void);

/**
 * @brief	Sets the new position to globalPos structure
 * @param x: value to set for position of axis x, -1 to leave previous value
 * @param y: value to set for position of axis y, -1 to leave previous value
 * @param z: value to set for position of axis z, -1 to leave previous value
 * @param fi: value to set for position of axis fi, -1 to leave previous value
 */
void SetGlobalPostition(int32_t x, int32_t y, int32_t z, int32_t fi);

/**
 * @brief 	Reads switchIndication value
 * @retval	Position structure filled with current switchIndication values
 */
EndOfLine getEndOfLine();

/**
 * @brief	Sets the new position to switchIndication structure
 * @param axis: Which axis end sensors send the indication, or which we want clear
 * @param set: Which end of axis, 0 for clearing, 1 for home position, 2 for end position
 */
void setEndOfLine(axes_t axis, uint8_t set);

/**
 * @brief Will clear all the values in switchIndication structure
 */
void clearEndOfLine();

#endif /* POSITION_H_ */
