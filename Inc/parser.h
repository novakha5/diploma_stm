/**
  ******************************************************************************
  * @file           : parser.h
  * @brief          : Header file for parsing the incoming string from USB.
  * 				  Also contains functions for sending error message or
  * 				  current global position.
  ******************************************************************************
  * Created on: 12. 6. 2020
  * Author: novakha5
  ******************************************************************************
*/

#ifndef PARSER_H_
#define PARSER_H_

#include "string.h"
#include <stdint.h>
#include "stepsToRun.h"

#define NUMBER_OF_AXES 4
#define MAX_NUM_LEN 10

 /**
  * @brief Takes incoming line, parse it and sets the position
  * @param line: Line to parse
  */
void parseLine(const uint8_t* line);
/**
 * @param dest: Array where to put split parts
 * @param strToParse: String to parse
 * @param delim: Delimit by which string should be split
 * @param num: Place where to store information about number of split parts
 */
void myStrtok(char dest[NUMBER_OF_AXES][MAX_NUM_LEN], const uint8_t* strToParse, char delim, int* num);
/**
 * @brief Convert string value to long integer if there are numbers from beginning
 * @param str: String to convert
 * @param end: Pointer to store next position after conversion
 * @retval Converted number if found any, otherwise 0
 */
long myDecadicStrtol(const char *str, char **end);
/**
 * @brief Counts number of characters till '\0'
 * @param str: Character array to count
 * @retval Length of string
 */
int myStrlen(const uint8_t* str);
/**
 * @brief Write the error to USB
 */
void errorMessage();
/**
 * @brief Sends actual global position to the USB
 */
void sendPosition();

/**
 * @brief Will parse manual message and set variables
 * @param str: String containing manual message
 */
void parseManualMessage(const char *str);

#endif /* PARSER_H_ */
