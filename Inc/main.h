/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_dma.h"

#include "stm32f4xx_ll_exti.h"

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* Private defines -----------------------------------------------------------*/
#define DIR_FI_Pin GPIO_PIN_0
#define DIR_FI_GPIO_Port GPIOC
#define DIR_Z_Pin GPIO_PIN_1
#define DIR_Z_GPIO_Port GPIOC
#define ENABLE_MOTORS_Pin GPIO_PIN_3
#define ENABLE_MOTORS_GPIO_Port GPIOC
#define DIR_X_Pin GPIO_PIN_1
#define DIR_X_GPIO_Port GPIOA
#define DIR_Y_Pin GPIO_PIN_2
#define DIR_Y_GPIO_Port GPIOA
#define TIM_Y_Pin GPIO_PIN_5
#define TIM_Y_GPIO_Port GPIOA
#define TIM_Z_Pin GPIO_PIN_6
#define TIM_Z_GPIO_Port GPIOA
#define TIM_X_Pin GPIO_PIN_8
#define TIM_X_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define LIMIT_X_Pin GPIO_PIN_15
#define LIMIT_X_GPIO_Port GPIOA
#define LIMIT_X_EXTI_IRQn EXTI15_10_IRQn
#define LIMIT_Y_Pin GPIO_PIN_10
#define LIMIT_Y_GPIO_Port GPIOC
#define LIMIT_Y_EXTI_IRQn EXTI15_10_IRQn
#define LIMIT_Z_Pin GPIO_PIN_11
#define LIMIT_Z_GPIO_Port GPIOC
#define LIMIT_Z_EXTI_IRQn EXTI15_10_IRQn
#define LIMIT_FI_Pin GPIO_PIN_12
#define LIMIT_FI_GPIO_Port GPIOC
#define LIMIT_FI_EXTI_IRQn EXTI15_10_IRQn
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define TIM_FI_Pin GPIO_PIN_9
#define TIM_FI_GPIO_Port GPIOB

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
