# Automated HW component tester - User manual (PCB) ver 1.0

The PCB is used for timing motors for CNC for automatic HW component tester. This part of the project consists of the HW as well as SW part. 


## PCB connections

For using this PCB with motors, the drivers are needed too. Look for the datasheet of your drivers for correct wire connection. 

In the picture below, the PCB is displayed.

![](PCB_numbered_small.png)

**The PCB consists of several parts:**

1. The USB input/output - This is used for communication to the computer as well as charging up the PCB
2. Processor part - STM32F401
3. Reset button
4. Programming header
5. Optional USART header (an alternative to USB) - not supported
6. Outputs for X, Y, Z motors
   1. Positive pulse 
   2. Negative pulse (GND)
   3. Positive direction
   4. Negative direction (GND)
   5. Positive enable
   6. Negative Enable (GND)
7. Output for Fi motor
   1.  Pulse
   2. Direction
   3. Vcc (5V)
   4. Enable
8. Power up for end switches
   1. 24V (Can be between 6V to 36V)
   2. GND
9. End switches inputs/outputs
   1. and 6. Vcc (24V)
   2. and 5. The signal from end switch
   3. and 4. GND


### Connecting motors

In this version, the motors X, Y and Z are connected using GND as reference. But the enable wire is connected to the same one as the Phii motor. For the Phii motor, the driver reference is 5V. Therefore for Phi, the enable pin needs to be in logical 1, but for other motors, it needs to be logical 0. In future revisions of the PCB, the enable pin needs to be added for every motor separately.

The solution is not to connect the enable part for X, Y and Z motors. As unconnected, the drivers detect it as enabled. (See the reference of the driver since it can differ in different drivers)

Other parts need to be connected to the respective inputs of the driver.


### Connecting end switches

The PCB is designed for the use of the normally open inductive end switch.  For every axis, there are two slots for the end switch. The program internally checks the running motor's direction, so it does not need to add separately for both ends. Every switch of this type has three wires (Vcc, Signal, GND) see the switch documentation to wire it correctly.


## USB communication

For connecting the PCB to the computer,  the USB-A cable is needed. Usually, it would be USB A-B cable to connect it to standard pc connector. There need to be installed virtual com port from STM provider: [STM Virtual COM port](https://my.st.com/content/my_st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-utilities/stsw-link009.license=1518543103787.html#get-software) for the communication.

After a successful connection and driver installation, the communication is possible using some serial port communication terminal. This PCB has been designed to communicate primarily with **Automatic HW tester - PC part**.

### Communication messages - receiving

USB communication is based on the position sending. The PC part of the program will calculate the position, where CNC needs to be moved, and it will send the actual steps which need to be done from home position. The STM will calculate the number of steps for motors from its current position and start the timers. After arriving at the desired position, it will send the coordinates back to the pc program.

For example, if the pc part needs the CNC on the position x = 1500 steps and y = 100 steps, the message will look like this:

> 1500,100,0,0\r

The STM will calculate the needed steps from the current position to the needed one and move the motors. Then it will send the new current position (in the same format) back.

The motors X and Y can be run simultaneously. The position Z is for bringing the motor for Phi axis to the buttons, and it could be a problem while moving the other axis so any changes needed there will wait till the axis X and Y are finished. The axis Phi will activate when any other axis has completed all the steps. 

There are other messages supported. The position messages return the current position if the motors are not in motion.

> POSITION\r

The Home command will navigate the robot close to the home position (10,10,10,0) and run at the slow speed until the sensors react. Then it will update the home position according to the sensors.

> HOME\r

There is also a need for the manual adjustment of the position of first tested components. That is why the commands for slow movement in one direction is needed.

> \<axis\>+\r 

> \<axis\>-\r 

The axis specifies, which one should move, the + (right, up, forward, clockwise) and - (left, down, backwards, anticlockwise) specify the direction in which the movement should be done. The message will look, for example, like:

> X+\r

This command will slowly move the X-axis to the right until the stop message comes, or the sensor detects the working area's end. The stop message looks like:

> STOP\r

For the Phi axis, the **F+\r** is used.

### Communication messages - returning

After finished request, the robot will send the current position as confirmation. The only exceptions are the manual requests, which are ended by STOP, the position will be sent as a response to the stop message.

If the position, which is sent by the USB is out of boundaries, the response will say:

> Position is out of boundaries\r

If the pc does not wait for the response and send another position before the STM finishes the steps, it will respond with:

> Motors are busy\r

And the message will be ignored.

For the manual movement, there is a possibility of receiving a request to move in the direction that is already on the boundaries. In this case, the direction of motion is not possible; the STM returns the message:

> "The direction is not possible\r

If the manual movement runs to the boundary, the motion is stopped by the sensor the same way as by the stop message.

If the message received from USB is not in the correct format, the STM will response with:

> Incorrect message\r

## Changing constants

Some constant changes in the software part will need to be done if different CNC is used. For example, in the **init.h** file, there are constants for the dimensions of the CNC machine.

> #define MACHINE_RANGE_X 	

> #define MACHINE_RANGE_Y 	

> #define MACHINE_RANGE_Z 	

> #define MACHINE_RANGE_FI 	

These constants are in the number of steps since they do not know how big one step is, the size should be written as a number of full steps times the micro-steps to keep the change of micro-steps comfortable.

In the same **init.h** file there is a possibility to change the micro-steps. They are set on the motor drivers, but the program needs to be notified to adjust the speed curve and the range calculation. The same settings need to be set in the PC part of the program. All axes' settings should be the same except for the Phi axis, which can be adjusted separately since the motion is direct.

> #define MICRO_STEPS

> #define MICRO_STEPS_FI

There are also some constants which could be changed in **motors.c** file. They are used for calculating the period of the PWM for motor control and are responsible for the speed of the motors and its acceleration and slowing down. In case that there are different motors placed or the machine's weight is changed, it needs to be set to a different value.

> #define changingSteps	(*How many steps should be slowed down before the maximum speed*)

Set in the format of a number of full steps times the micro-steps - currently 400*micro to set it for two spins of motor

> #define maxPeriod	(*Longest period - for the slowest steps*)

> #define minPeriod	(*Period for the maximum speed*)

The maximum speed is set concerning the micro-steps settings.

> #define maxFreq	(*Frequency for low speed*)

> #define minFreq	(*Frequency for maximum speed*)

Frequency is calculated from the period set - should not be changed.

### Updating the program

After the constants are changed, the program needs to be compiled and loaded to the PCB. The program has been build by Atollic arm tools, and the settings are part of the project. For the loading the project to the PCB, there are multiple possibilities. For all of them, there is STLink HW needed. The easiest way to use it is to utilise Atollic functionality.

## Inside working

The frame of the program was generated from STMCubeMx software, and the FreeRtos is used.

Three main threads are running, and interrupts are used for other functionality. The threads are:

1. Watchdog control
2. USB communication (parsing the incoming messages)
3. Motor control

For the watchdog, the thread only clears the counter every 100 ms. 

As for the USB communication, the thread will control the incoming buffer if the sentence ended with \r was received and then parse it.  If the valid message is received, the thread will calculate the steps, to get there and set it to the structure.

Motor control thread will set the right timers to generate PWM output. More in the **Motor timers** chapter.

The interrupts are mainly used for EXTI input pins which are connected to the end switches. The interrupt will stop the timer for the right axis and set the correct position to the structure (to 0 or maximum range of the shaft, depending on the motor direction).

### Motor timers

There are six timers dedicated to controlling the PWM output for the motors. The table below shows which timer does what.

| Axis | Timer for PWM | Timer for counting steps        |
| ---- | ------------- | ------------------------------- |
| X    | TIM1          | TIM4 slave to TIM1_TRGO (ITR0)  |
| Y    | TIM2          | TIM9 slave to TIM2_TRGO (ITR0)  |
| Z    | TIM3          | TIM4 slave to TIM3_TRGO (ITR2)  |
| Phi  | TIM11         | TIM9 slave to TIM11_TRGO (ITR3) |

The chaining of the timers for the motor control can be seen in the picture below:

![](Readme_img/Diagram_small.png)



Since only the axis X and Y can run simultaneously, there is no need for more timers. The timers 4 and 9 can be switched to monitor PWM outputs of the master timers for the active axis and then change to the other.

The timers 4 and 9 will rise the interrupt when the steps are finished, and the interrupt handler will stop further PWM generation.

The speed of the PWM is set by the thread which updates the speed in the loop. The calculation is based on the steps from the beginning as a linear function until the *changingSteps* are reached, and then the speed is maximum. When the number of steps reaches the *changingSteps* constant or lower, the linear function is repeated with the opposite sign. 

The calculation also counts with a smaller number of steps then two times *changingSteps*, in that case, the acceleration will go till there are fewer steps from the beginning then till the end, then the sign is switched. 

**The Motor speed curve**

Currently, the timers are set (by the dividers) to 125 kHz. The speed curve is calculated using frequency. The movement starts as ten slow steps which are done at a constant speed, and then it performs a linear speed-up for next 400×micro-steps, which correspond to two turns of the motor, at which point the maximum speed is reached. The slow-down starts when two turns of the motor are remaining, and uses the same linear function in reverse, to slow down the movement. The speed can be changed by changing the constants in **motors.c** file. However, it is recommended to change only the period ones and the number of "changingSteps". The period is understood as the number of 125 kHz ticks.

Since the speed is not inserted into the timers by frequency but by the integer period, the curve is not smooth. However, it is sufficient enough for the motor to be able to run smoothly. The current curve for the 20 000 steps can be seen below:

![](Readme_img/Speed_curve_small.png)

The orange curve is period, relative to the timer settings, the blue curve is the actual frequency of the PWM output.

### Storing position and steps

There is the **globalPos** structure (for storing the current position) and the **switchIndication** structure (for the switches states), which is controlled by **position (.c/.h)** files. The reading and writing to it is done by getters and setters and is protected by a mutex. The same way is stored information about the rise of the signal from the end switch.

Similarly, there are stored the steps needed to be done for reaching the position. **stepsToRun (.c/.h)** files control this. The structures are **StepsToRun** and the **RemainingSteps** (used for the movements longer than timer's counter).

## TODO - future changes

There are some problems which should be addressed in the future rebuilds.

### PCB 

1. **Separating enable signals**

   The explanation can be found in the **Connecting motor**'s part.

2. **Pull-down resistors for output**

   The pull-downs are enabled in the software. However, they are not active while booting up. In that case, the idle state is logical 1 instead of logical 0, enabling the motors with a possibility to send few spurious steps.

   The program looks for the home position after the boot-up, so it does not affect the position. The movement would not be that much of the problem either since for the 8 micro-steps settings, which are currently used, one step corresponds to 2.5 μm movement.

3. **Change the resistor arrays for the separate resistors**

   There are two types of resistor arrays used. However, the correct parts could be problematic to obtain. It would be beneficial to change them to separate commonly used resistors in future production.

4. **Replace incorrect footprints**

   For the buffers, the footprint is smaller then it should be. There is also a problem with the size of the through-holes for the 4-pin barrier terminal.

5. **Flip the order of the pinout for the motors**

   The pinout for the motor driver is the same as the output from the board. It means that the connection is cross-wise.

6. **Connect barrier terminals**

   The barrier terminals can be connected to reduce the size of the PCB.

7. **Add inputs for the touch sensors in the home position**

   The touch sensor in the home position would bring more precise positioning since the proximity sensors can differ in the results up to 6 mm. The touch sensor would also solve the starting problem (however unlikely) to distinguish the upper and lower positions.

### Firmware

There is a suspicion of memory corruption in the firmware. Sometimes, the code could not change the counter timer input (change the ITR from one to another). That results in the slowest motion without the speed curve and counting the steps. The program recovers once the robot border is reached and the sensor indicates the side.

The problem was not located. The suspicion is the simultaneous use of HAL libraries with RTOS since the HAL uses the dynamic memory allocation.